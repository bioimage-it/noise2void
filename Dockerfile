FROM nvidia/cuda:11.2.2-base-ubuntu18.04

WORKDIR /app

COPY . /app

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install wget && \
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
    chmod +x Miniconda3-latest-Linux-x86_64.sh && \
    bash Miniconda3-latest-Linux-x86_64.sh -b -p /app/miniconda3 && \
    rm Miniconda3-latest-Linux-x86_64.sh && \
    /app/miniconda3/bin/conda init && \
    /app/miniconda3/bin/conda install python=3.6 && \
    /app/miniconda3/bin/conda install tensorflow-gpu=1.14 keras=2.2.4 && \
    /app/miniconda3/bin/pip install n2v

ENV PATH="/app/miniconda3/bin:$PATH"
ENV PATH="/app:$PATH"

CMD ["bash"]
